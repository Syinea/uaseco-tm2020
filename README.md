Hello Guys,
this is my Fork of UASECO for TM2020 Trackmania https://trackmania.com/

Latest Version: ![Latest Stable Version](https://i.ibb.co/sJff95w/Screenshot-2.png) for TM2020

IMPORTANT! My Version changes has Nothing to do with undeflabs changes, undeflabs last change was 0.9.6 on 2019-03-02 - 17:59 check his Website

I want to get it to run with TM2020.
This Fork/Branch is tested on Windows Server. To USE IT follow the installation instructions of UASECO https://www.uaseco.org/
You should setup fresh server controllers for TM2020, its not recommended to use existing installations from TM2.

To see it running join my Lol Server. You can find my TM2020 Server by searching for `Syinea` 
(if they are running, at the moment im searching for SPONSORS to host my Servers, have no money over to host myself over some Time)

Working Alternatives to UASECO are PyPlanet https://github.com/PyPlanet/PyPlanet and EvoSC https://github.com/EvoTM/EvoSC

BestNoob, u.B.m and Syinea have worked on this fork so far

This FORK is OPEN for Feedback.

Now a quick Guide made by Hoerli
[![HoerliMovie](http://i.imgur.com/7YTMFQp.png)](https://www.youtube.com/watch?v=WRX5qs5uX-w "uAseco Guide by Hoerli")




Official UASECO Part



[![Donate](https://img.shields.io/badge/paypal-donate-yellow.svg)](https://paypal.me/UASECO)
[![Latest Stable Version](https://poser.pugx.org/undeflabs/uaseco/v/stable?format=flat-square)](https://packagist.org/packages/undeflabs/uaseco)
[![License](https://poser.pugx.org/undeflabs/uaseco/license?format=flat-square)](https://packagist.org/packages/undeflabs/uaseco)


![logo](https://www.uaseco.org/media/github-uaseco-logo.jpg)



UASECO is a fork of the XAseco2/1.03 controller for Trackmania and has been overhauled to support the Modescript Gamemodes of Trackmania² in Maniaplanet and changes to run with PHP/7.x or newer.
Only Modescript Gamemodes of Trackmania² are supported, there is no support included for the legacy Gamemodes of Trackmania².



What means UASECO?
==================

ASECO is an abbreviation of Automatic SErver COntrol, the prefix U at UASECO stays for undef, which follows the same naming convention initiated by Xymph for XAseco.

This project was forked in May 2014 from XAseco2/1.03 release and was mixed with parts/ideas from MPAseco and ASECO/2.2.0c, for supporting the Trackmania² Modescript Gamemodes from the Maniaplanet/3+ update.

Many parts has been rewritten in 2017 again because of the ModeScript API changes for the Maniaplanet/4 update.



Official website
================

For more details, please visit the official website at https://www.UASECO.org/

The official forum for UASECO can be found at http://forum.maniaplanet.com/viewforum.php?f=522
